//function - constructor
function Hamburger(size, stuffing) {
    try {
        if (!size || !stuffing) {
            throw new HamburgerException("Size and stuffing required");
        } else if (size.type != "size" || stuffing.type != "stuffing") {
            throw new HamburgerException("Wrong size or stuffing");
        }
        this.size = size;
        this.stuffing = stuffing;
        this.toppings = [];
        console.log("Your hamburger is " + size.size + " with " + stuffing.stuffing + " stuffing prepared");
    } catch (err) {
        console.error(err.message);
    }
}

//Constant sizes, stuffing, toppings...
Hamburger.SIZE_SMALL = {
    type: "size",
    size: "Small",
    price: 50,
    cal: 20
};
Hamburger.SIZE_LARGE = {
    type: "size",
    size: "Large",
    price: 100,
    cal: 40
};
Hamburger.STUFFING_CHEESE = {
    type: "stuffing",
    stuffing: "cheese",
    price: 10,
    cal: 20
};
Hamburger.STUFFING_SALAD = {
    type: "stuffing",
    stuffing: "salad",
    price: 20,
    cal: 5
};
Hamburger.STUFFING_POTATO = {
    type: "stuffing",
    stuffing: "potato",
    price: 15,
    cal: 10
};
Hamburger.TOPPING_MAYO = {
    type: "topping",
    topping: "mayo",
    price: 20,
    cal: 5
};
Hamburger.TOPPING_SPICE = {
    type: "topping",
    topping: "spice",
    price: 15,
    cal: 0
};

Hamburger.prototype.addTopping = function (topping) {
    try {
        if (!topping) {
            throw new HamburgerException("Topping required!");
        } else if (this.toppings.includes(topping)) {
            throw new HamburgerException("Duplicate topping");
        }
        this.toppings.push(topping);
        console.log(topping.topping + " topping added");
    } catch (err) {
        console.error(err.message);
    }
}

Hamburger.prototype.removeTopping = function (topping) {
    try {
        if (!topping) {
            throw new HamburgerException("Specify topping to remove");
        } else if (!this.toppings.includes(topping)) {
            throw new HamburgerException("No such topping in burger!");
        }
        this.toppings.splice(this.toppings.indexOf(topping), 1);
        console.log(topping.topping + " topping removed");
    } catch (err) {
        console.error(err.message);
    }
}

Hamburger.prototype.getToppings = function () {
    return this.toppings;
}

Hamburger.prototype.getSize = function () {
    return this.size.size;
}

Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
}

Hamburger.prototype.calculatePrice = function () {
    let totalPrice = this.size.price + this.stuffing.price;
    for (let topping of this.toppings) {
        totalPrice += topping.price;
    }
    return totalPrice;
}

Hamburger.prototype.calculateCalories = function () {
    let totalCalories = this.size.cal + this.stuffing.cal;
    for (let topping of this.toppings) {
        totalCalories += topping.cal;
    }
    return totalCalories;
}

//Custom UserException constructor
function HamburgerException(message) {
    this.name = "hamburgerException";
    this.message = message;
}

//POSITIVE CASES:
// маленький гамбургер с начинкой из сыра
let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
console.log(hamburger);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1

//NEGATIVE CASES:
console.log("-->trying to pass wrong parameters to Hamburger constructor:");
// не передали обязательные параметры
var h2 = new Hamburger(); // => HamburgerException: no size given

// передаем некорректные значения, добавку вместо размера
var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// => HamburgerException: invalid size 'TOPPING_SAUCE'

// добавляем много добавок
var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// HamburgerException: duplicate topping 'TOPPING_MAYO'


//ADDITIONAL LOGIC FOR USER INTERACTION (NOT COMPLETE!)
let size, stuffing, topping, burger;

const choises = document.querySelectorAll(".choise");
for (let choise of choises) {
    choise.addEventListener("click", makeChoise);
}

function makeChoise(i) {
    i.target.classList.toggle("chosen");
    switch (i.target.dataset.choise) {
        case "big": {
            size = Hamburger.SIZE_LARGE;
            break;
        }
        case "small": {
            size = Hamburger.SIZE_SMALL;
            break;
        }
        case "cheese": {
            stuffing = Hamburger.STUFFING_CHEESE;
            break;
        }
        case "salad": {
            stuffing = Hamburger.STUFFING_SALAD;
            break;
        }
        case "potato": {
            stuffing = Hamburger.STUFFING_POTATO;
            break;
        }
        case "mayo": {
            burger.addTopping(Hamburger.TOPPING_MAYO);
            burgerInfo();
            break;
        }
        case "spice": {
            burger.addTopping(Hamburger.TOPPING_SPICE);
            burgerInfo();
            break;
        }
    }
}

const makeBurgerBtn = document.querySelector(".make-burger");
makeBurgerBtn.addEventListener("click", () => {
    burger = new Hamburger(size, stuffing);
    burgerInfo();
});

function burgerInfo() {
    document.querySelector(".burger-info").innerHTML = ` ${burger.size.size} burger with ${burger.stuffing.stuffing} prepared!<br>
        Calories: ${burger.calculateCalories()} <br>
        Price: $ ${burger.calculatePrice()}`;
}
